from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Aviliani Pramestya' # TODO Implement this
birth_date = date(1997, 12, 4)
# Create your views here
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return date.today().year - birth_year
